A friendly bot which posts the lunch menu of canteens located at the campus [Garching Forschungszentrum](http://www.forschung-garching.tum.de/) (GFZ) of the [Technical University of Munich](http://www.tum.de/) (TUM) to this Telegram channel here (http://t.me/lunchgfz).

💬 Mondays at 10:00 o‘clock the bot posts the _weekly lunch menu_ as multiple pictures for every canteen.
Additionally, from Monday till Friday every morning at nine o‘clock, the bot posts the _lunch menu of the current day_ as text.
Note that the bot only displays changing dishes and the daily posts only contain the _daily changing main meals_ (no side dishes). However, the canteens always sell various other foods like salads, baked goods, or pastries.

*Currently the following canteens are supported:*
  *Studentenwerk München:*
  ☕️️SCG • StuCafé inside the [Mensa Garching](http://www.studentenwerk-muenchen.de/mensa/standorte-und-oeffnungszeiten/garching/)
  🔩️MW • StuCafé Boltzmannstraße inside the Department of [Mechanical Engineering](http://www.mw.tum.de/) (Maschinenwesen)
  Ⓜ️️MG • [Mensa Garching](http://www.studentenwerk-muenchen.de/mensa/standorte-und-oeffnungszeiten/garching/)
  *Private:*
  💻FMI • [Canteen Wilhelm Gastronomie](http://www.wilhelm-gastronomie.de/tum-garching) inside the Department of [Mathematics](http://www.ma.tum.de/) and [Informatics](http://www.in.tum.de/) (FMI)
  ⚛️IPP •  [Canteen Konradhof Catering](https://konradhof-catering.de/ipp/) at the [Max-Planck-Institut für Plasmaphysik](http://www.ipp.mpg.de/) (IPP)

*Legend:*
  🍏 = Vegan dish
  🧀 = Vegetarian dish
  🍗 = Dish with meat or fish
  🌍 = International dish
  🎎 = Traditional dish
  💥 = Special dish

  🍝 = main dish menu
  🍚 = side dish menu

  ❎ = Some of the StuCafés do not have a daily dish on some days in the week. However, they still have their other food on sale on that days.
  🚫 = Sometimes the canteens miss to upload their menu for some weeks, although they nevertheless have one. So you have to visit the canteen to look at the menu.

*Ingredients:*
  🏅    Certified Quality - Bavaria (Zertifizierte Qualität - Bayern)
  🌊    Marine Stewardship Council

  🎨    with dyestuff (Farbstoffe)
  ➿    with preservative (Konservierungsmittel)
  🌬️    with antioxidant (Antioxidationsmittel)
  😋    with flavor enhancers (Geschmacksverstärker)
  🜍     sulphured (Sulphate)
  ⬛    blackened (olive) (geschwärzt (Olive))
  🕯     waxed (gewachst)
  🦴    with phosphate (Phosphate)
  🍬    with sweeteners (Süßungsmittel)
  🍼    contains a source of phenylalanine (Phenylalanin)
  🍭    with sugar and sweeteners (Zucker und Süßungsmittel)
  🍫    with cocoa-containing grease (Fett mit Kakao)
  🍮    with gelatin (Gelantine)
  🍸    with alcohol (Alkohol)

  🧀    meatless dish (fleischlos)
  🍏    vegan dish (vegan)
  🐖    with pork (Schweinefleisch)
  🐂    with beef (Rind)
  🐮    with veal (Kalb)
  🐓    with poultry (Geflügel)
  🐗    with wild meat (Wildfleish)
  🐑    with lamb (Lamm)
  🥘    with garlic (Knoblauch)
  🥚    with chicken egg (Hühnerei)
  🥜    with peanut (Erdnuss)
  🐟    with fish (Fisch)

  🥣    with gluten-containing cereals (Getreide mit Gluten)
  🌾    with wheat (Weizen)
  GlR   with rye (Roggen)
  GlG   with barley (Gerste)
  GlH   with oats (Hafer)
  GlD   with spelt (Spelz)
  🦀    with crustaceans (Krustentiere)
  🐺    with lupines (Lupine)
  🥛    with milk and lactose (Milch und Laktose)

  🌰    with shell fruits (Schalenfrüchte)
  ScM   with almonds (Mandeln)
  ScH   with hazelnuts (Haselnüsse)
  ScW   with Walnuts (Walsüssen)
  ScC   with cashew nuts (Cashewnüssen)
  ScP   with pistachios (Pistazien)
  🌱    with sesame seeds (Sesamsamen)
  🌭    with mustard (Senf)
  🌿    with celery (Sellerie)
  🍢    with soy (Soja)
  Sw    with sulfur dioxide and sulfites (Schwefeldioxide und Sulfite)
  🐌    with mollusks (Weichtiere)

  ⚠️ Do not rely on the correctness of the ingredients. The parser makes errors, especially if the parsed PDF is malformed.

⚠️ I do not guarantee the correctness of the bot or the posted content (Irrtümer vorbehalten).

🌐 The bot‘s _project homepage_ [here](https://gitlab.com/raabf/lunchgfz-telegram) ([issues](https://gitlab.com/raabf/lunchgfz-telegram/issues)).
For parsing the PDFs, the bot makes use of [eat-api](https://github.com/srehwald/eat-api/) ([issues](https://github.com/srehwald/eat-api/issues)).

🛑 Problems should go to the _issue tracker_ of the respective project homepage.
