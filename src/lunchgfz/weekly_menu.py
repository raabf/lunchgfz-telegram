import abc
import datetime as dt
import logging
from pathlib import Path
import re
import time
from typing import Optional, Sequence, Tuple, Union
from urllib import error as urlerror
from urllib import request as urlrequest
import warnings

from lxml import html
import requests
from telethon import TelegramClient
import wand
from wand.image import Image  # pylint: disable=ungrouped-imports

from lunchgfz.base_menu import BaseMenu, FMIBase, IPPBase, SWMBase
from lunchgfz.common import (AlreadyPublishedException, Automatic, DayWithoutMenuException, DayWithoutMenuWarning,
                             client_connect, default_cache_dir, get_monday, next_weekday)

PDF_DOWNLOAD_RETRY_TRIES = 10
PDF_DOWNLOAD_RETRY_SLEEP = 150  # in seconds


class WebpageParsingException(Exception):
    pass


class DownloadFailedException(Exception):
    pass


class WeeklyMenu(BaseMenu, metaclass=abc.ABCMeta):

    def __init__(self, *args, monday: Optional[dt.date] = Automatic, friday: Optional[dt.date] = Automatic,
                 week_nr: Optional[int] = Automatic, cache_dir: Optional[Path] = Automatic, is_initial: bool = True,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.monday = get_monday(dt.date.today()) if not monday else monday
        self.friday = next_weekday(self.monday, 4) if not friday else friday
        self.week_nr = int(self.monday.strftime("%V")) if not week_nr else week_nr
        self.cache_dir = default_cache_dir if not cache_dir else cache_dir
        self._log: logging.Logger
        self.is_initial = is_initial
        """Must be set by subclass"""

    def post(self, channel: str, pictures: Optional[Sequence[Path]], client: TelegramClient = Automatic):

        telethon_client = client_connect() if not client else client

        msg = '{}{} 🗓W{:0>2d} 📅{}–{}'.format(
            self.icon, self.abbr, self.week_nr, self.monday.strftime("%d.%m."), self.friday.strftime("%d.%m.%y"))

        if pictures is None:
            msg += "\n\n🚫 No menu was published online."

            self._log.debug("Sending message%s: %s", '' if self.publish else " (DEACTIVATED)", msg)
            if self.publish:
                telethon_client.send_message(channel, msg)

        elif pictures:
            for i, pic_path in enumerate(pictures):

                if len(pictures) >= 2 and i == 0:
                    caption = msg + " 🍝"
                elif len(pictures) >= 2 and i == 1:
                    caption = msg + " 🍚"
                else:
                    caption = msg

                self._log.debug("Sending file with caption '%s'%s: %s.", caption,
                                '' if self.publish else " (DEACTIVATED)", str(pic_path))
                if self.publish:
                    telethon_client.send_file(channel, str(pic_path), caption=caption)
        else:
            raise DayWithoutMenuException("{}: No menu is published online for week {:0>2d}.".format(
                self.abbr, self.week_nr))

    def obtain_pictures(self) -> Optional[Tuple[Path, ...]]:

        failed_file_path = Path(self.cache_dir, "failed_{}_{}_w{}.marker".format(self.abbr, self.friday.year,
                                                                                 self.week_nr))

        if not self.is_initial and not failed_file_path.is_file():
            raise AlreadyPublishedException("{} menu for w{} was already successfully published in a previous run. "
                                            "No need for further execution.".format(self.abbr, self.week_nr))

        for i in range(PDF_DOWNLOAD_RETRY_TRIES):
            pdf = None
            try:
                pdf = self.obtain_pdf()

            except WebpageParsingException:
                pass

            if pdf is None:
                self._log.error("Function did return None. Assume that resource is not available.")
                self._log.error("Failed to find the PDF — aborting.")

                # create a file to store the information that the fetching of the pdf has failed.
                self._log.debug("PDF not available online. create file %s.", failed_file_path)
                failed_file_path.touch(exist_ok=True)

                return None

            if pdf and pdf.is_file():
                break
            else:
                if pdf and not pdf.is_file():
                    emsg = "The PDF do not exist on disk or is not a file: {}".format(pdf)
                    self._log.error(emsg)
                self._log.error("Connection try no.%d failed. Wait for %dsec.", i, PDF_DOWNLOAD_RETRY_SLEEP)
                time.sleep(PDF_DOWNLOAD_RETRY_SLEEP)

        else:
            emsg = "Finally failed to obtain the PDF – aborting."

            # create a file to store the information that the fetching of the pdf has failed.
            self._log.debug("PDF not available online. create file %s.", failed_file_path)
            failed_file_path.touch(exist_ok=True)

            self._log.critical(emsg)
            raise DownloadFailedException(emsg)

        if failed_file_path.is_file():
            failed_file_path.unlink()

        pictures: Tuple[Path, ...] = self.convert_pdf(pdf)
        if not all(p.is_file() for p in pictures):
            emsg = "Some pictures do not exist on disk or are not files: {}".format(pictures)
            self._log.critical(emsg)
            raise Exception(emsg)

        return pictures

    @abc.abstractmethod
    def obtain_pdf(self) -> Optional[Path]:
        pass

    @abc.abstractmethod
    def convert_pdf(self, menu_pdf: Path) -> Tuple[Path, ...]:
        pass


class IPPWeeklyMenu(WeeklyMenu, IPPBase):

    def __init__(self, publish: bool, monday: Optional[dt.date] = Automatic,  # pylint: disable=too-many-arguments
                 friday: Optional[dt.date] = Automatic, week_nr: Optional[int] = Automatic,
                 cache_dir: Optional[Path] = Automatic, is_initial: bool = True) -> None:
        super().__init__(publish=publish, monday=monday, friday=friday, week_nr=week_nr, cache_dir=cache_dir,
                         is_initial=is_initial)
        self._log = logging.getLogger("IPPWeeklyMenu")
        self.webpage = "https://konradhof-catering.com/ipp/"

    def obtain_pdf(self) -> Optional[Path]:
        self._log.info("Processing week %02d starting with day %s", self.week_nr, self.monday)

        # Get webpage and parse it
        page = requests.get(self.webpage)
        # get html tree
        tree = html.fromstring(page.content)
        # get url of current pdf menu
        xpath_query = list(set(
            tree.xpath("//a[contains(@href, 'KW-{:d}')]/@href".format(self.week_nr)) +
            tree.xpath("//a[contains(@href, 'KW_{:d}')]/@href".format(self.week_nr)) +
            tree.xpath("//a[contains(@href, 'KW-{:02d}')]/@href".format(self.week_nr)) +
            tree.xpath("//a[contains(@href, 'KW_{:02d}')]/@href".format(self.week_nr))
        ))
        self._log.info("Querying KW-%d and KW_%d which returns %s", self.week_nr, self.week_nr, xpath_query)

        if len(xpath_query) > 2:
            emsg = "Parsing of webpage failed: {}".format(self.webpage)
            self._log.error(emsg)
            raise WebpageParsingException(emsg)

        # For spacial days the IPP has sometimes an own menu card, which results in two founded pdfs. This was only
        # seen on Aschermittwoch
        elif len(xpath_query) > 1:
            emsg = "Parsing of webpage probably failed (two results): {}".format(self.webpage)
            self._log.error(emsg)
            warnings.warn(emsg)

        pdf_url = xpath_query[0] if xpath_query and len(xpath_query) <= 2 else None
        # TODO probably allow the return and posting of multiple pdfs

        # if len(xpath_query) == 0 we assume that the KW is not available, but it could be also a parsing error
        # https://regex101.com/r/uKsv3D/
        if not pdf_url or not re.search(r"KW[-_.\s]?\d+", pdf_url, flags=re.IGNORECASE):
            self._log.error("No menu in form of a pdf available for week %02d.", self.week_nr)
            return None

        menu_pdf_path = Path(self.cache_dir, "ipp_w{:0>2d}.pdf".format(self.week_nr))

        self._log.debug("Downloading %s to %s", pdf_url, menu_pdf_path)
        urlrequest.urlretrieve(pdf_url, str(menu_pdf_path))

        return menu_pdf_path

    def convert_pdf(self, menu_pdf: Path) -> Tuple[Path, ...]:
        menu_jpg_path = Path(self.cache_dir, "ipp_w{:0>2d}.jpg".format(self.week_nr))
        menu_jpg_path0 = Path(self.cache_dir, "ipp_w{:0>2d}-0.jpg".format(self.week_nr))
        menu_jpg_path1 = Path(self.cache_dir, "ipp_w{:0>2d}-1.jpg".format(self.week_nr))

        self._log.info("Convent %s to %s", menu_pdf, menu_jpg_path)

        with Image(filename=str(menu_pdf), resolution=300,
                   background=wand.color.Color("white")) as img:
            with img.convert('jpg') as converted:
                converted.background_color = wand.color.Color("white")
                converted.alpha_channel = "background"
                converted.save(filename=str(menu_jpg_path))

        # check if pdf has two pages (every page is an own jpg file)
        if menu_jpg_path1.is_file():
            return menu_jpg_path0, menu_jpg_path1

        return (menu_jpg_path,)


class FMIWeeklyMenu(WeeklyMenu, FMIBase):

    def __init__(self, publish: bool, monday: Optional[dt.date] = Automatic,  # pylint: disable=too-many-arguments
                 friday: Optional[dt.date] = Automatic, week_nr: Optional[int] = Automatic,
                 cache_dir: Optional[Path] = Automatic, is_initial: bool = True) -> None:
        super().__init__(publish=publish, monday=monday, friday=friday, week_nr=week_nr, cache_dir=cache_dir,
                         is_initial=is_initial)
        self._log = logging.getLogger("FMIWeeklyMenu")
        self.webpage = "https://www.wilhelm-gastronomie.de"

    def obtain_pdf(self) -> Optional[Path]:
        self._log.info("Processing week %02d starting with day %s", self.week_nr, self.monday)

        # Get webpage and parse it

        # get web page of bistro
        page = requests.get(self.webpage)
        # get html tree
        self._log.debug("Processing downloaded page.")
        tree = html.fromstring(page.content)
        # get url of current pdf menu
        query_string = "//a[contains(@href, 'Speisekarte')]/@href"
        xpath_query = tree.xpath(query_string)
        self._log.info("Querying %s and get %s", query_string, xpath_query)

        if not xpath_query:
            emsg = "Parsing of webpage failed: {}".format(self.webpage)
            self._log.error(emsg)
            raise WebpageParsingException(emsg)

        # test at https://regex101.com/r/bNhVH4/1
        week_number_regex = re.compile("KW[^a-zA-Z]*0*{:d}".format(self.week_nr), re.IGNORECASE)
        pdf_url: str
        for pdf_url in xpath_query:
            if week_number_regex.search(pdf_url):
                self._log.debug("Found 'KW' and week number %d in %s", self.week_nr, pdf_url)
                break
        else:
            self._log.error("No menu in form of a pdf available for week %02d.", self.week_nr)
            return None

        menu_pdf_path = Path(self.cache_dir, "fmi_w{:0>2d}.pdf".format(self.week_nr))

        self._log.debug("Downloading %s to %s", pdf_url, menu_pdf_path)

        urlrequest.urlretrieve(pdf_url, str(menu_pdf_path))

        return menu_pdf_path

    def convert_pdf(self, menu_pdf: Path) -> Tuple[Path, ...]:
        menu_jpg_path = Path(self.cache_dir, "fmi_w{:0>2d}.jpg".format(self.week_nr))

        self._log.info("Convent %s to %s", menu_pdf, menu_jpg_path)

        with Image(filename=str(menu_pdf), resolution=300) as img:
            img.trim()
            img.save(filename=str(menu_jpg_path))

        return (menu_jpg_path,)


class SWMWeeklyMenu(WeeklyMenu, SWMBase):

    def __init__(self, publish: bool, location: Union[str, int],  # pylint: disable=too-many-arguments
                 monday: Optional[dt.date] = Automatic, friday: Optional[dt.date] = Automatic,
                 week_nr: Optional[int] = Automatic, cache_dir: Optional[Path] = Automatic,
                 is_initial: bool = True) -> None:
        super().__init__(publish=publish, monday=monday, friday=friday, week_nr=week_nr, cache_dir=cache_dir,
                         is_initial=is_initial, location=location)
        self._log = logging.getLogger("SWMWeeklyMenu" + str(self.location_id))

    def obtain_pdf(self) -> Optional[Path]:
        self._log.info("Processing week %02d from %s–%s.", self.week_nr, self.monday, self.friday)

        menu_url = ("http://www.studentenwerk-muenchen.de/mensa/speiseplan/"
                    "speiseplan-kw_{}-{:0>2d}_{}_-de.pdf").format(self.monday.year, self.week_nr, self.location_id)

        menu_pdf_path = Path(self.cache_dir, "swm_{}_w{:0>2d}.pdf".format(self.location_id, self.week_nr))

        self._log.debug("Downloading %s to %s", menu_url, menu_pdf_path)

        try:
            urlrequest.urlretrieve(menu_url, str(menu_pdf_path))
        except urlerror.HTTPError as err:
            if err.code == 404:  # HTTP Error 404: Not Found
                msg = "{}: {}: {}".format(self.abbr, str(err), menu_url)
                self._log.error(msg)
                warnings.warn(msg, DayWithoutMenuWarning)
                return None

            raise err

        return menu_pdf_path

    def convert_pdf(self, menu_pdf: Path) -> Tuple[Path, ...]:
        menu_jpg_path = Path(self.cache_dir, "swm_{}_w{:0>2d}.jpg".format(self.location_id, self.week_nr))
        menu_jpg_path0 = Path(self.cache_dir, "swm_{}_w{:0>2d}-0.jpg".format(self.location_id, self.week_nr))
        menu_jpg_path1 = Path(self.cache_dir, "swm_{}_w{:0>2d}-1.jpg".format(self.location_id, self.week_nr))

        self._log.info("Convent %s to %s", menu_pdf, menu_jpg_path)

        with Image(filename=str(menu_pdf), resolution=300,
                   background=wand.color.Color("white")) as img:
            with img.convert('jpg') as converted:
                converted.save(filename=str(menu_jpg_path))

        if menu_jpg_path.exists():
            return (menu_jpg_path,)

        return menu_jpg_path0, menu_jpg_path1
