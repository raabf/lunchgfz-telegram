#!/usr/bin/env python3

import datetime as dt
import logging
import os
from pathlib import Path
import random
import sys
from typing import Any, Callable, List, Optional, Tuple, Union, cast
import warnings
from warnings import WarningMessage  # type: ignore

import click
from dotmap import DotMap

from eat_api.entities import Ingredients
from lunchgfz.bon_appetit import BON_APPETIT_LIST
from lunchgfz.common import (CHANNEL_NAME, DAY_WITHOUT_MENU_EXITCODE, DOWNLOAD_FAILED_EXITCODE, client_connect,
                             client_disconnect, get_monday)
from lunchgfz.daily_menu import DailyMenu, FMIDailyMenu, IPPDailyMenu, SWMDailyMenu
from lunchgfz.ingredients_map import INGREDIENTS_EMOJI, INGREDIENTS_GERMAN
from lunchgfz.weekly_menu import (AlreadyPublishedException, DownloadFailedException, FMIWeeklyMenu, IPPWeeklyMenu,
                                  SWMWeeklyMenu)

LOG_LEVELS = ("notset", "debug", "info", "warning", "error", "critical")

dir_path = Path(os.path.dirname(os.path.realpath(__file__)))  # pylint: disable=invalid-name

log = logging.getLogger()  # pylint: disable=invalid-name


def __validate_verbosity(ctx: click.Context, param: click.Parameter, value: str) -> Optional[int]:
    """Parse the verbosity from the command line."""
    # pylint: disable=unused-argument
    if value is None:
        return None
    ret = None

    try:
        ret = int(value)
    except (TypeError, ValueError):
        try:
            # we directly try to get the level number instead going trough
            # log_levels so there could be more valid ones then in log_levels
            ret = int(getattr(logging, value.upper()))
        except (AttributeError, TypeError):
            raise click.BadParameter("must be a number or one of " + ", ".join(LOG_LEVELS))

    return ret


def bon_appetit_message(dlog: logging.Logger) -> str:
    """
    Creates a string with “Bon Appitit” in different languages and language information.
    
    Args:
        dlog: A logger to send debug informaiton

    Returns:
        A single string line ready to send.
    """
    bon_appetit = random.choice(BON_APPETIT_LIST)

    bon_appetit_msg = "{} {}{}{}".format(
        bon_appetit.flag,
        bon_appetit.sentence,
        " ({})".format(bon_appetit.pronunciation) if bon_appetit.pronunciation else '',
        " — {}".format(bon_appetit.note) if bon_appetit.note else '',
    )

    dlog.debug("Bon appetit message: %s", bon_appetit_msg)
    return bon_appetit_msg


ClickCallback = Optional[  # pylint: disable=invalid-name
    Callable[[click.Context, Union[click.Option, click.Parameter], Union[bool, int, str]], Any]]

validate_verbosity: ClickCallback = cast(ClickCallback, __validate_verbosity)


@click.group()
@click.option("--verbosity", callback=validate_verbosity, help="Default is warning.")
@click.option("--verbose", "-v", count=True, help="Be more verbose (level -5). Can be repeated.")
@click.option("--quiet", "-q", count=True, help="Be more quiet (level +5). Can be repeated.")
@click.option("--logfile", "-l", help=("the path to the file to which the module will log to. "
                                       "Leave it emtpy to log to stdout."))
@click.option("--publish", "-p", is_flag=True, default=False,
              help="Publishes the output to telegram (Default is to only parse the sources).")
@click.option("--date", "-d", help="The date of the requested lunch menu. Format 31.12.2017. Defaults to today.")
@click.pass_context  # pylint: disable=too-many-arguments
def cli(ctx: click.Context, logfile: Optional[str] = None, verbosity: Optional[int] = None,
        verbose: Optional[int] = None, quiet: Optional[int] = None, publish: bool = None, date: str = None) -> None:
    """Parsing of the command line arguments and initialization of the logger and its verbosity."""

    level_list = list()
    default_level = "WARNING"

    log_to = dict()  # type: dict
    if logfile:
        log_to = {"filename": logfile}
        logging.captureWarnings(True)
    else:
        log_to = {"stream": sys.stdout}
        logging.captureWarnings(False)
    logging.basicConfig(format="%(asctime)-15s %(levelname)-7s %(name)-10s %("
                               "funcName)-16s %(message)s", **log_to)

    if verbosity:
        level = verbosity
        log.debug("Set log level to %s (%s)", logging.getLevelName(level), str(level))
        level_list.append(level)

    if verbose:
        level = getattr(logging, default_level) - (verbose * 5)
        log.debug("Set log level to %s (%s)", logging.getLevelName(level), str(level))
        level_list.append(level)

    if quiet:
        level = getattr(logging, default_level) + (quiet * 5)
        log.debug("Set log level to %s (%s)", logging.getLevelName(level), str(level))
        level_list.append(level)

    if len(level_list) > 1:
        log.warning("Multiple verbosity levels defined! Finally using %s (%s).",
                    logging.getLevelName(level_list[0]), str(level_list[0]))

    level_list.append(getattr(logging, default_level))  # add default value

    log.setLevel(level_list[0])

    if publish:
        log.warning("The output gets published to the telegram.")
    else:
        log.warning("The output gets not published to the telegram.")
    ctx.obj.publish = publish

    if date:
        ctx.obj.date = dt.datetime.strptime(date, "%d.%m.%Y")
    else:
        ctx.obj.date = dt.date.today()


@cli.group(help="Post weekly PDF menu as picture.", invoke_without_command=True)
@click.option('--initial/--no-initial', default=True,
              help="If the initial fetch for a PDF failed it can be tried again. For retries messages are only pushed"
                   "when the PDF gets available and only if the initial try failed. Make sure that inital is only used"
                   "once per week and is the first call of this function.")
@click.pass_context
def weekly(ctx: click.Context, initial: bool):
    ctx.obj.initial = initial

    if ctx.invoked_subcommand is None:
        wlog = logging.getLogger("WeeklyBatch")

        wlog.info("All weekly modules will be executed.")

        canteen_list = [
            ("StuCafe inside the Mensa Garching", swm_weekly, "stucafe-garching"),
            ("StuCafe Boltzmannstraße (MW)", swm_weekly, "stucafe-boltzmannstr"),
            ("Mensa Garching", swm_weekly, "mensa-garching"),
            ("FMI Canteen", fmi_weekly, None),
            ("IPP Canteen", ipp_weekly, None),
        ]
        err_list = []

        for desc, func, loc in canteen_list:
            try:
                wlog.info("%s will be executed.", desc)
                if loc is None:
                    ctx.invoke(func)
                else:
                    ctx.invoke(func, location=loc)
            except Exception as err:  # pylint: disable=broad-except
                err_list.append(err)

        if err_list:
            emsg = "Errors occurred: {}".format(err_list)
            wlog.error(emsg)
            print(str(emsg), file=sys.stderr)
            # TODO probably separate between non critical exceptions like DayWithoutMenuException and bugs
            raise err_list[0]

    else:
        ctx.obj.no_batch = True


@cli.group(help="Post daily the parsed menu as text.", invoke_without_command=True)
@click.pass_context
def daily(ctx: click.Context):
    if ctx.invoked_subcommand is None:
        dlog = logging.getLogger("DailyBatch")

        dlog.info("All weekly modules will be executed.")

        msg_list, all_warns, err_list = _daily_canteens(ctx)

        ## add date header

        today = ctx.obj.date
        date_msg = '{}{}   📅{}'.format(
            DailyMenu.weekday_icons[today.weekday()],
            today.strftime("%a"), today.strftime("%d.%m.%y")
        )

        ## add bon appetit line

        msg_list.append(bon_appetit_message(dlog))

        line_bottom = "▁▁▁▁▁▁▁▁▁▁\n"
        full_msg = "\n\n{}".format(line_bottom).join(msg_list)
        full_msg = date_msg + '\n' + line_bottom + full_msg

        ## Sending the message

        client = client_connect()
        dlog.debug("Sending message%s: %s", '' if ctx.obj.publish else " (DEACTIVATED)", full_msg)
        if ctx.obj.publish:
            client.send_message(CHANNEL_NAME, full_msg)

        for warn in all_warns:
            print(warn, type(warn))
            warnings.warn_explicit(warn.message, warn.category, warn.filename, warn.lineno)
        if not err_list and all_warns:
            exit(DAY_WITHOUT_MENU_EXITCODE)

        if err_list:
            emsg = "Errors occurred: {}".format(err_list)
            dlog.error(emsg)
            print(str(emsg), file=sys.stderr)
            # TODO probably separate between non critical exceptions like DayWithoutMenuException and bugs
            raise err_list[0]

    else:
        ctx.obj.no_batch = True


def _daily_canteens(ctx: click.Context) -> Tuple[List[str], List[WarningMessage], List[Exception]]:
    """
    Iterates through the canteens and creates the menus string for them.

    Args:
        ctx: click context for inter method communication. Must contain:
            - publish
            - date

    Returns:
        The message as list with a string per line and all Warnings and Exeptions.
    """
    dlog = logging.getLogger("DailyBatch")

    canteen_list = [
        ("StuCafe inside the Mensa Garching", SWMDailyMenu, "stucafe-garching"),
        ("StuCafe Boltzmannstraße (MW)", SWMDailyMenu, "stucafe-boltzmannstr"),
        ("Mensa Garching", SWMDailyMenu, "mensa-garching"),
        ("FMI Canteen", FMIDailyMenu, None),
        ("IPP Canteen", IPPDailyMenu, None),
    ]
    err_list: List[Exception] = []
    msg_list: List[str] = []
    # TODO remove ignore when warnings.WarningMessage is detected
    all_warns: List[WarningMessage] = []

    ## Get menu for every canteen

    canteen: DailyMenu

    for desc, cls, loc in canteen_list:
        try:
            dlog.info("%s will be executed.", desc)
            if loc is None:
                canteen = cls(publish=ctx.obj.publish, today=ctx.obj.date)
            else:
                canteen = cls(publish=ctx.obj.publish, location=loc, today=ctx.obj.date)

            menus = canteen.obtain_menus()
            with warnings.catch_warnings(record=True) as warns:
                msg_list.append(canteen.create_message(menus, with_date=False))

                if warns is not None:
                    all_warns += warns

        except Exception as err:  # pylint: disable=broad-except
            err_list.append(err)

    return msg_list, all_warns, err_list


@weekly.command("fmi", help="Download weekly pdf and post FMI menu.")
@click.pass_context
def fmi_weekly(ctx: click.Context) -> None:
    fmi = FMIWeeklyMenu(publish=ctx.obj.publish,
                        monday=get_monday(ctx.obj.date) if ctx.obj.date else None,
                        is_initial=ctx.obj.initial)
    try:
        pictures = fmi.obtain_pictures()
    except AlreadyPublishedException as err:
        log.debug(str(err))
        print(str(err))
        return

    try:
        fmi.post(CHANNEL_NAME, pictures)
    except DownloadFailedException as err:
        if ctx.obj.no_batch:
            print(str(err), file=sys.stderr)
            exit(DOWNLOAD_FAILED_EXITCODE)
        else:
            raise err


@weekly.command("ipp", help="Download weekly pdf and post IPP menu.")
@click.pass_context
def ipp_weekly(ctx: click.Context) -> None:
    ipp = IPPWeeklyMenu(publish=ctx.obj.publish,
                        monday=get_monday(ctx.obj.date) if ctx.obj.date else None,
                        is_initial=ctx.obj.initial)
    try:
        pictures = ipp.obtain_pictures()
    except AlreadyPublishedException as err:
        log.debug(str(err))
        print(str(err))
        return

    try:
        ipp.post(CHANNEL_NAME, pictures)
    except DownloadFailedException as err:
        if ctx.obj.no_batch:
            print(str(err), file=sys.stderr)
            exit(DOWNLOAD_FAILED_EXITCODE)
        else:
            raise err


@weekly.command("swm", help="Download and post weekly pdf menu of Studentenwerk München canteens.")
@click.argument('location')
@click.pass_context
def swm_weekly(ctx: click.Context, location: str) -> None:
    swm = SWMWeeklyMenu(publish=ctx.obj.publish, location=location,
                        monday=get_monday(ctx.obj.date) if ctx.obj.date else None,
                        is_initial=ctx.obj.initial)
    try:
        pictures = swm.obtain_pictures()
    except AlreadyPublishedException as err:
        log.debug(str(err))
        print(str(err))
        return

    try:
        swm.post(CHANNEL_NAME, pictures)
    except DownloadFailedException as err:
        if ctx.obj.no_batch:
            print(str(err), file=sys.stderr)
            exit(DOWNLOAD_FAILED_EXITCODE)
        else:
            raise err


@daily.command("fmi", help="Download daily data and post FMI menu as text.")
@click.pass_context
def fmi_daily(ctx: click.Context) -> None:
    fmi = FMIDailyMenu(publish=ctx.obj.publish, today=ctx.obj.date)
    menus = fmi.obtain_menus()
    with warnings.catch_warnings(record=True) as warns:
        fmi.post(CHANNEL_NAME, menus)

        if warns is not None:
            for warn in warns:
                warnings.warn_explicit(warn.message, warn.category, warn.filename, warn.lineno)
            if ctx.obj.no_batch and warns:
                exit(DAY_WITHOUT_MENU_EXITCODE)
        else:
            raise Exception("catch_warnings is None")


@daily.command("ipp", help="Download daily data and post IPP menu as text.")
@click.pass_context
def ipp_daily(ctx: click.Context) -> None:
    ipp = IPPDailyMenu(publish=ctx.obj.publish, today=ctx.obj.date)
    menus = ipp.obtain_menus()
    with warnings.catch_warnings(record=True) as warns:
        ipp.post(CHANNEL_NAME, menus)

        if warns is not None:
            for warn in warns:
                warnings.warn_explicit(warn.message, warn.category, warn.filename, warn.lineno)
            if ctx.obj.no_batch and warns:
                exit(DAY_WITHOUT_MENU_EXITCODE)
        else:
            raise Exception("catch_warnings is None")


@daily.command("swm", help="Download daily data and post selected Studentenwerk München location menu as text.")
@click.argument('location')
@click.pass_context
def swm_daily(ctx: click.Context, location: str) -> None:
    swm = SWMDailyMenu(publish=ctx.obj.publish, location=location, today=ctx.obj.date)
    menus = swm.obtain_menus()
    with warnings.catch_warnings(record=True) as warns:
        swm.post(CHANNEL_NAME, menus)

        if warns is not None:
            for warn in warns:
                warnings.warn_explicit(warn.message, warn.category, warn.filename, warn.lineno)
            if ctx.obj.no_batch and warns:
                exit(DAY_WITHOUT_MENU_EXITCODE)
        else:
            raise Exception("catch_warnings is None")


@cli.group(help="Print ingredients mappings.", invoke_without_command=False)
@click.pass_context
def ingredients(_ctx: click.Context) -> None:
    pass


@ingredients.command("icons", help="Print ingredients as icons to description mappings.")
@click.pass_context
def icons(_ctx: click.Context) -> None:
    line = "{:5} {}".format("Icon", "Description")
    print(line)
    print("----------------------------------")

    for code, desc in Ingredients.ingredient_lookup.items():
        icon = INGREDIENTS_EMOJI[code]
        if not icon:
            icon = code
        line = "{:5} {} ({})".format(icon, desc, INGREDIENTS_GERMAN[code])
        print(line)


@ingredients.command("codes", help="Print ingredients as codes to description mappings.")
@click.pass_context
def codes(_ctx: click.Context) -> None:
    line = "{:5} {}".format("Code", "Description")
    print(line)
    print("----------------------------------")

    for code, desc in Ingredients.ingredient_lookup.items():
        line = "{:5} {} ({})".format(code, desc, INGREDIENTS_GERMAN[code])
        print(line)


if __name__ == "__main__":
    try:
        cli(obj=DotMap())  # pylint: disable=no-value-for-parameter, unexpected-keyword-arg
    finally:
        client_disconnect()
