#!/bin/bash
# Should be executed by cron every week
# Attention folders are very specific to my environment
# put the following line into crontab
#0    10  *  *  Mon /opt/gitlab_cd/lunchgfz-telegram/scripts/cron.sh --verbosity=debug --publish weekly
#30   10  *  *  Mon-Fri /opt/gitlab_cd/lunchgfz-telegram/scripts/cron.sh --verbosity=debug --publish daily

export PATH="~/bin:~/.local/bin:$PATH"
export PIPENV_VENV_IN_PROJECT=true

abspath() {
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

SCRIPTNAME=$(basename $0)
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PROJECT_DIR="$(abspath "$SCRIPTPATH/..")"
LOG_DIR="/var/log"

# old method via pyenv
VIRTUALENV_DIR="/home/raabf/.pyenv/versions/lunchgfz"
#. $VIRTUALENV_DIR/bin/activate

cd "$PROJECT_DIR"

# Remember to run pipenv sync before
PYTHONPATH=$PROJECT_DIR/src LC_ALL=C.UTF-8 LANG=C.UTF-8 pipenv run python -m lunchgfz --logfile=$LOG_DIR/lunchgfz.log $@

