import collections

BonAppetit = collections.namedtuple("BonAppetit", ["language", "dialect", "sentence", "pronunciation", "flag", "note"])

# sources:
#   https://www.facebook.com/GastroGeniusGmbH/posts/517527848268589
#   https://www.chefkoch.de/forum/2,22,258755/quot-Guten-Appetit-quot-in-allen-Sprachen.html?page=1
#   https://www.rund-ums-baby.de/forenarchiv/mehrsprachig/Guten-Appetit-In-vielen-Sprachen-bitte_1876.htm

BON_APPETIT_LIST = [
BonAppetit(language="Afrikaans", dialect="", sentence="Smaaklike ete!", pronunciation="", flag="🇿🇦", note=""),
BonAppetit(language="Afrikaans", dialect="", sentence="Lekker eet!", pronunciation="", flag="🇿🇦", note=""),
BonAppetit(language="Albanian", dialect="", sentence="T'boftë mire!", pronunciation="", flag="🇦🇱", note=""),
BonAppetit(language="Basque", dialect="", sentence="On egin!", pronunciation="", flag="🏴󠁥󠁳󠁰󠁶󠁿", note=""), # TODO flag not available as image
BonAppetit(language="Bulgarian", dialect="", sentence="Добър апетит!", pronunciation="Dobãr apetit!", flag="🇧🇬", note=""),
BonAppetit(language="Catalan", dialect="", sentence="Bon profit!", pronunciation="", flag="🏴󠁥󠁳󠁣󠁴󠁿", note=""), # TODO flag not available as image
BonAppetit(language="Catalan", dialect="", sentence="Que vagi de gust!", pronunciation="", flag="🏴󠁥󠁳󠁣󠁴󠁿", note=""), # TODO flag not available as image
BonAppetit(language="Chinese", dialect="Cantonese", sentence="食飯", pronunciation="sihk faahn", flag="🇨🇳", note="eat"),
BonAppetit(language="Chinese", dialect="Hakka", sentence="慢慢吃", pronunciation="mang-mang sik", flag="🇨🇳", note=""),
BonAppetit(language="Chinese", dialect="Mandarin", sentence="慢慢吃!", pronunciation="mànmàn chī!", flag="🇨🇳", note="eat slowly"),
BonAppetit(language="Croatian", dialect="", sentence="Dobar tek!", pronunciation="", flag="🇭🇷", note=""),
BonAppetit(language="Czech", dialect="", sentence="Dobrou chuť!", pronunciation="", flag="🇨🇿", note=""),
BonAppetit(language="Danish", dialect="", sentence="Velbekomme!", pronunciation="", flag="🇩🇰", note=""),
BonAppetit(language="Dutch", dialect="", sentence="Smakelijk!", pronunciation="", flag="🇳🇱", note=""),
BonAppetit(language="Dutch", dialect="", sentence="Smakelijk eten!", pronunciation="", flag="🇳🇱", note=""),
BonAppetit(language="Dutch", dialect="", sentence="Eet Smakelijk!", pronunciation="", flag="🇳🇱", note=""),
BonAppetit(language="English", dialect="", sentence="Bon appetit!", pronunciation="", flag="🇬🇧", note=""),
BonAppetit(language="English", dialect="", sentence="Enjoy your meal!", pronunciation="", flag="🇺🇸", note=""),
BonAppetit(language="Esperanto", dialect="", sentence="Bonan apetiton!", pronunciation="", flag="⭐💚", note=""), # TODO correct flag not available yet
BonAppetit(language="Filipino", dialect="", sentence="Kakain na!", pronunciation="", flag="🇵🇭", note="Let us eat."),
BonAppetit(language="Finnish", dialect="", sentence="Hyvää ruokahalua!", pronunciation="", flag="🇫🇮", note=""),
BonAppetit(language="French", dialect="", sentence="Bon appétit!", pronunciation="", flag="🇫🇷", note=""),
BonAppetit(language="German", dialect="", sentence="Guten Appetit!", pronunciation="", flag="🇩🇪", note=""),
BonAppetit(language="German", dialect="", sentence="Mahlzeit!", pronunciation="", flag="🇩🇪", note=""),
BonAppetit(language="German", dialect="Bavarian", sentence="An Guadn!", pronunciation="", flag="🇩🇪", note=""),
BonAppetit(language="German", dialect="Bavarian", sentence="Lass da’s schmegga!", pronunciation="", flag="🇩🇪", note=""),
BonAppetit(language="Greek", dialect="modern", sentence="Καλή όρεξη!", pronunciation="Kalí óreksi!", flag="🇬🇷", note=""),
BonAppetit(language="Greenlandic", dialect="", sentence="Nerilluarisi", pronunciation="", flag="🇬🇱", note=""),
BonAppetit(language="Hungarian", dialect="", sentence="Jó étvágyat!", pronunciation="", flag="🇭🇺", note=""),
BonAppetit(language="Icelandic", dialect="", sentence="Verði þér að góðu", pronunciation="", flag="🇮🇸", note=""),
BonAppetit(language="Indonesian", dialect="", sentence="Selamat makan!", pronunciation="", flag="🇮🇩", note=""),
BonAppetit(language="Italian", dialect="", sentence="Buon appetito!", pronunciation="", flag="🇮🇹", note=""),
BonAppetit(language="Japanese", dialect="", sentence="どうぞめしあがれ", pronunciation="douzo meshiagare", flag="🇯🇵", note="enjoy your meal - said by the cook/chef"),
BonAppetit(language="Japanese", dialect="", sentence="いただきます", pronunciation="itadakimasu", flag="🇯🇵", note="said before a meal by those eating it"),
BonAppetit(language="Kurdish", dialect="", sentence="Noº be!", pronunciation="", flag="🏴󠁩󠁲󠀱󠀶󠁿", note=""), # TODO flag not available as image
BonAppetit(language="Latin", dialect="", sentence="Bonum appetitionem!", pronunciation="", flag="🌿🦅🌿", note=""),
BonAppetit(language="Latvian", dialect="", sentence="Labu apetīti", pronunciation="", flag="🇱🇻", note=""),
BonAppetit(language="Lithuanian", dialect="", sentence="Gero apetito!", pronunciation="", flag="🇱🇹", note=""),
BonAppetit(language="Lithuanian", dialect="", sentence="Skanaus!", pronunciation="", flag="🇱🇹", note=""),
BonAppetit(language="Luxembourgish", dialect="", sentence="Gudden Appetit!", pronunciation="", flag="🇱🇺", note=""),
BonAppetit(language="Macedonian", dialect="", sentence="Пријатно јадење", pronunciation="Priyatno yadenye", flag="🇲🇰", note=""),
BonAppetit(language="Norwegian", dialect="", sentence="Vær så god!", pronunciation="", flag="🇳🇴", note=""),
BonAppetit(language="Polish", dialect="", sentence="Smacznego!", pronunciation="", flag="🇵🇱", note=""),
BonAppetit(language="Portuguese", dialect="", sentence="Bom apetite!", pronunciation="", flag="🇵🇹", note=""),
BonAppetit(language="Romanian", dialect="", sentence="Poftă bună!", pronunciation="", flag="🇷🇴", note=""),
BonAppetit(language="Russian", dialect="", sentence="Приятного аппетита!", pronunciation="Prijatnovo appetita!", flag="🇷🇺", note=""),
BonAppetit(language="Serbian", dialect="", sentence="Пријатно!", pronunciation="Prijatno!", flag="🇷🇸", note=""),
BonAppetit(language="Slovak", dialect="", sentence="Dobrú chuť!", pronunciation="", flag="🇸🇰", note=""),
BonAppetit(language="Slovenian", dialect="", sentence="Dober tek!", pronunciation="", flag="🇸🇮", note=""),
BonAppetit(language="Somali", dialect="", sentence="Ha kuu macaanaato", pronunciation="", flag="🇸🇴", note=""),
BonAppetit(language="Spanish", dialect="", sentence="¡Buen provecho!", pronunciation="", flag="🇪🇸", note=""),
BonAppetit(language="Spanish", dialect="", sentence="¡Buen apetito!", pronunciation="", flag="🇪🇸", note=""),
BonAppetit(language="Spanish", dialect="", sentence="¡Que aproveche!", pronunciation="", flag="🇪🇸", note=""),
BonAppetit(language="Swedish", dialect="", sentence="Smaklig måltid!", pronunciation="", flag="🇸🇪", note=""),
BonAppetit(language="Swiss", dialect="", sentence="En Guete!", pronunciation="", flag="🇨🇭", note=""),
BonAppetit(language="Tahitian", dialect="", sentence="Tamaa maitai", pronunciation="", flag="🇵🇫", note=""), # flag for French Polynesia
]
