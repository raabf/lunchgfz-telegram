# lunch gfz

<!--[![coverage report](https://gitlab.com/raabf/lunchgfz-telegram/badges/master/coverage.svg)](https://gitlab.com/raabf/lunchgfz-telegram/commits/master)-->
[![build status](https://gitlab.com/raabf/lunchgfz-telegram/badges/master/build.svg)](https://gitlab.com/raabf/lunchgfz-telegram/commits/master)
[![license](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://www.tldrlegal.com/l/gpl-3.0)
[![Maintenance](https://img.shields.io/maintenance/yes/2019.svg)]()

A friendly bot which posts the lunch menu of canteens located at the campus [Garching Forschungszentrum](http://www.forschung-garching.tum.de/) (GFZ) of the [Technical University of Munich](http://www.tum.de/) (TUM) to this Telegram channel [lunchgfz](http://t.me/lunchgfz).

💬 Mondays at 10:00 o‘clock the bot posts the _weekly lunch menu_ as multiple pictures for every canteen.
Additionally, from Monday till Friday every morning at 10:30 o‘clock, the bot posts the _lunch menu of the current day_ as text.
Note that the bot only displays changing dishes and the daily posts only contain the _daily changing main meals_ (no side dishes). However, the canteens always sell various other foods like salads, baked goods, or pastries.

## Currently the following canteens are supported:
 - **Studentenwerk München:**
    + ☕️SCG • StuCafé inside the [Mensa Garching](http://www.studentenwerk-muenchen.de/mensa/standorte-und-oeffnungszeiten/garching/)
    + 🔩️MW • StuCafé Boltzmannstraße inside the Department of [Mechanical Engineering](http://www.mw.tum.de/) (Maschinenwesen)
    + Ⓜ️️MG • [Mensa Garching](http://www.studentenwerk-muenchen.de/mensa/standorte-und-oeffnungszeiten/garching/)
 - **Private:**
    + 💻FMI • [Canteen Wilhelm Gastronomie](http://www.wilhelm-gastronomie.de/tum-garching) inside the Department of [Mathematics](http://www.ma.tum.de/) and [Informatics](http://www.in.tum.de/) (FMI)
    + ⚛️IPP •  [Canteen Konradhof Catering](https://konradhof-catering.de/ipp/) at the [Max-Planck-Institut für Plasmaphysik](http://www.ipp.mpg.de/) (IPP)


## Legend:
 + 🍏 = Vegan dish
 + 🧀 = Vegetarian dish
 + 🍗 = Dish with meat or fish
 + 🌍 = International dish
 + 🎎 = Traditional dish
 + 💥 = Special dish
 + 🍝 = main dish menu
 + 🍚 = side dish menu
 + ❎ = Some of the StuCafés do not have a daily dish on some days in the week. However, they still have their other
 food on sale on that days.
 + 🚫 = Sometimes the canteens miss to upload their menu for some weeks, although they nevertheless have one. So you
 have to visit the canteen to look at the menu.


## Notes

⚠️ I do not guarantee the correctness of the bot or the posted content (Irrtümer vorbehalten).

🌐 The bot‘s _project homepage_ [here](https://gitlab.com/raabf/lunchgfz-telegram) ([issues](https://gitlab.com/raabf/lunchgfz-telegram/issues)).
For parsing the PDFs, the bot makes use of [eat-api](https://github.com/srehwald/eat-api/) ([issues](https://github.com/srehwald/eat-api/issues)).

🛑Problems should go to the _issue tracker_ of the respective project homepage.

## Requirements

  + only tested on linux
  + Python >= 3.6
  + ImageMagick
  + MagickWand, C API @ ImageMagick
  + pdftotext

Install dependencies:

    pip3 install -r requirements.txt

## Usage

    cd ./src
    python3 -m lunchgfz --verbosity=debug --help
    python3 -m lunchgfz --verbosity=debug daily --help
    python3 -m lunchgfz --verbosity=debug weekly --help
