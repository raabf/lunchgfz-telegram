import abc
import datetime as dt
import locale
import logging
import numbers
import random
import warnings
from typing import Dict, Iterable, List, Optional, Sequence, Set, Tuple, Union, cast

from telethon import TelegramClient

from eat_api.entities import Dish, Menu
from eat_api.menu_parser import FMIBistroMenuParser, IPPBistroMenuParser, StudentenwerkMenuParser
from lunchgfz.base_menu import BaseMenu, FMIBase, IPPBase, SWMBase
from lunchgfz.common import Automatic, DayWithoutMenuWarning, atleast, client_connect, get_monday, next_weekday
from lunchgfz.ingredients_map import INGREDIENTS_EMOJI

locale.setlocale(locale.LC_ALL, ('en_US', 'UTF-8'))


class DailyMenu(BaseMenu, metaclass=abc.ABCMeta):
    dish_type_icons = {
        # ATTENTION: You need a emoji compatible font to display the values of this dict or else they are invisible.
        # In this case do not modify the dict values to not accidientially delete the emojis.
        "vegan": '🍏',
        "vegetarian": '🧀',
        "meat": '🍗',
        "international": '🌍',
        "traditional": '🎎',
        "special": '💥',
        "fish": '🐟',
        "pig": '🐖',
        "ponies": '🐎',
        "cow": '🐄',
        "chicken": '🐔',
        "flower1": '🌸',
        "flower2": '🏵',
        "flower3": '🌺',
        "flower4": '🌻',
        "flower5": '🌼',
        "flower6": '🌷',
        "flower7": '🌹',
    }

    weekday_icons = {
        0: '🌑',
        1: '🌒',
        2: '🌓',
        3: '🌔',
        4: '🌕',
        5: '🌖',
        6: '🌗',
    }

    def __init__(self, *args, today: Optional[dt.date] = Automatic, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if isinstance(today, dt.datetime):
            today = today.date()
        self.today = dt.date.today() if not today else today
        self._log: logging.Logger
        """Must be set by subclass"""

    @abc.abstractmethod
    def get_dish_type_order(self, dishes: Sequence[Dish]) -> List[str]:
        pass

    @staticmethod
    def ingredients_icons_string(codes: Iterable[str]) -> str:
        """
        Get the ingredients as icons in a single string.

        Args:
            codes: A list of ingredients codes from class Ingredients.

        Returns:
            A single string with the emoji representation of the ingredients. If no emoji is spacified, the code is
            returned.
        """
        icons_str = ""
        prev_no_icon = False

        for code in codes:
            icon = INGREDIENTS_EMOJI[code]

            if icon:
                icons_str += icon
                prev_no_icon = False
            else:
                icons_str += (',' + code) if prev_no_icon else code
                prev_no_icon = True

        return icons_str

    @staticmethod
    def ingredients_intersection(ingr_list: List[Set[str]]) -> Tuple[Set[str], List[Set[str]]]:

        if len(ingr_list) == 1:
            return set(), ingr_list

        intersection: Set[str] = set.intersection(*ingr_list)

        individual = [set(ingr) - intersection for ingr in ingr_list]

        return intersection, individual

    def _get_today_dishes(self, menus: Dict[dt.date, Menu]) -> Optional[List[Dish]]:
        today_dishes: Optional[List[Dish]] = None

        if menus is None:
            return None

        try:
            today_dishes = menus[self.today].dishes
        except KeyError:
            self._log.info("No menu available for the day %s.", self.today.strftime("%a %d.%m.%y"))
            # It must not be an error since the canteen has probably not published the menu on his webpage.

            try:
                is_stucafe_garching = True if self.location_id == 524 else False  # type: ignore
            except AttributeError:
                is_stucafe_garching = False

            if is_stucafe_garching:
                # This is a special case with stucafe-garching. It seems to they have no dish on wednesday.
                # In such a case stucafe-boltzmannstr will have an empty node (today_dishes == [] here already)
                # In contrast, stucafe-garching will have no node so we have a key error. However, I assume this is on
                # purpose and not an error. So we set today_dishes = [] if at least three meals are available in the
                # same week, else it is an error (None).
                self._log.info(
                    "Special case stucafe garching. No entry for day %s. Check if that is on purpose or an error",
                    self.today.strftime("%a %d.%m.%y"))
                sun = get_monday(self.today) - dt.timedelta(1)
                if atleast((next_weekday(sun, wd) in menus for wd in range(0, 4)), times=3):
                    self._log.debug("Special case stucafe garching. We have enough dishes in the week.")
                    today_dishes = []
                else:
                    self._log.error("Special case stucafe garching. We have NOT enough dishes in the week.")
                    today_dishes = None
            else:
                self._log.error("Dish for the day %s not found. Some error must have occurred.",
                                self.today.strftime("%a %d.%m.%y"))
                today_dishes = None

        return today_dishes

    def create_message(self, menus: Dict[dt.date, Menu], with_date: bool = True) -> str:
        today_dishes = self._get_today_dishes(menus)

        if with_date:
            msg = '{}{}   {}{}   📅{}'.format(
                self.icon, self.abbr, self.weekday_icons[self.today.weekday()],
                self.today.strftime("%a"), self.today.strftime("%d.%m.%y")
            )
        else:
            msg = '{} {}'.format(self.icon, self.abbr)

        msg += "\n▔▔▔▔▔▔▔▔▔▔"

        if today_dishes == []:
            # This is a non-error state. Some stucafes have no dishes on some weekdays.
            msg += "\n❎ No daily dish available today."
        elif today_dishes is None:
            msg += "\n🚫 No menu is published online."
        else:
            ingredients_list = [dish.ingredients for dish in today_dishes]

            ingredients_intersection, ingredients_individual = self.ingredients_intersection(ingredients_list)

            for dish, ingredients, dish_type in zip(today_dishes, ingredients_individual,
                                                    self.get_dish_type_order(today_dishes)):
                if isinstance(dish.prices, numbers.Number):
                    price = " {:01.2f}€".format(dish.price)
                else:
                    price = " [{}]".format(dish.prices)

                ingredients_str = self.ingredients_icons_string(ingredients)

                msg += "\n{} {} {} {}\n".format(self.dish_type_icons[dish_type], dish.name, ingredients_str, price)

            if ingredients_intersection:
                msg += "\nAll menus: {}\n".format(self.ingredients_icons_string(ingredients_intersection))

            msg = msg[:-1]  # remove the last '\n'

        if today_dishes is None:
            warnings.warn("{}: No menu available for the day {}.".format(
                self.abbr, self.today.strftime("%a %d.%m.%y")), DayWithoutMenuWarning)

        self._log.debug("Constructed message: %s", msg)

        return msg

    def post(self, channel: str, menus: Dict[dt.date, Menu], client: TelegramClient = Automatic) -> None:

        msg = self.create_message(menus)

        self._log.debug("Sending message%s: %s", '' if self.publish else " (DEACTIVATED)", msg)
        telethon_client = client_connect() if not client else client

        if self.publish:
            telethon_client.send_message(channel, msg)

    @abc.abstractmethod
    def obtain_menus(self) -> Dict[dt.date, Menu]:
        pass


class IPPDailyMenu(DailyMenu, IPPBase):

    def __init__(self, publish: bool, today: Optional[dt.date] = Automatic) -> None:
        super().__init__(publish=publish, today=today)
        self._log = logging.getLogger("IPPDailyMenu")

    def get_dish_type_order(self, dishes: Sequence[Dish]) -> List[str]:
        return ["vegetarian", "traditional", "international", "special"]

    def obtain_menus(self) -> Dict[dt.date, Menu]:
        self._log.info("Processing day %s", self.today)

        ipp = IPPBistroMenuParser()
        menus = ipp.parse("ipp-bistro")  # type: ignore
        return cast(Dict[dt.date, Menu], menus)


class FMIDailyMenu(DailyMenu, FMIBase):

    def __init__(self, publish: bool, today: Optional[dt.date] = Automatic) -> None:
        super().__init__(publish=publish, today=today)
        self._log = logging.getLogger("FMIDailyMenu")

    def get_dish_type_order(self, dishes: Sequence[Dish]) -> List[str]:
        # TODO sometimes there is only a vegan dish and no vegetarian. <= 2 is quite a big hack and should be replaced.
        if len(dishes) <= 2:
            return ["vegan", "meat", "special"]

        return ["vegan", "vegetarian", "meat", "special"]

    def obtain_menus(self) -> Dict[dt.date, Menu]:
        self._log.info("Processing day %s", self.today)

        fmi = FMIBistroMenuParser()
        menus = fmi.parse("fmi-bistro")  # type: ignore
        return cast(Dict[dt.date, Menu], menus)


class SWMDailyMenu(DailyMenu, SWMBase):

    def __init__(self, publish: bool, location: Union[str, int], today: Optional[dt.date] = Automatic) -> None:
        super().__init__(publish=publish, today=today, location=location)
        self._log = logging.getLogger("SWMDailyMenu" + str(self.location_id))

    def get_dish_type_order(self, dishes: Sequence[Dish]) -> List[str]:
        dish_type_order = ["flower1", "flower2", "flower3", "flower4", "flower5", "flower6", "flower7"]
        random.shuffle(dish_type_order)
        return dish_type_order

    def obtain_menus(self) -> Dict[dt.date, Menu]:
        self._log.info("Processing day %s", self.today)

        swm = StudentenwerkMenuParser()
        menus = swm.parse(self.location)  # type: ignore
        return cast(Dict[dt.date, Menu], menus)
